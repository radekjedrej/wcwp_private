<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WCWP</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="https://use.typekit.net/rbx8sjx.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Readex+Pro:wght@300;400;500;600;700&display=swap" rel="stylesheet">
</head>
<body>
    <div>
        <h1>Sample title</h1>
        <label class="u-switch">
            <input class="u-switch__checkbox" checked type="checkbox">
            <div class="u-switch__slider"></div>
            <div class="u-switch__text">
                <span class="u-switch__unchecked">Day</span>
                <span class="u-switch__checked">Week</span>
            </div>
        </label>
        <input class="u-input" placeholder="example" type="text">
        <script src="{{ asset('js/app.js') }}"></script>
    </div>
</body>
</html>
